# The challenge

The challenge consists of implementing a solution in pure c for the problem of parsing a string into a mathematical expression, and then evaluating it. The following rules generally apply:

 - Numbers could be given as integers or decimals;
 - Only the four basic operations are supported, and are represented by the following signs: `+`, `-`, `*`, `/`;
 - Precedence rule of operations are respected normally: parenthesis have highest precedence, followed by multiplication and division, ending with summation and subtraction;
 - Space characters should be ignored whenever they appear in the string, but numbers are represented as a whole without space characters. For example, `3.14` would be a valid number, but `3.1 4` would not;
 - Sum and subtraction signs could be used to sign numbers or parenthesis, and in the presence of more than one of these, the classical sign rule should be applied. As an example, all of the following should evaluate to `1`: `--1`, `-(-1)`, `+ -(- +1)`, `-1/-1`, `-1/( -1)`, `+- 1/ -( -1)`. On the other hand, none of the following would be valid expressions: `**1`, `//1`, `1*/1`;

The person being challenged is responsible for providing means of compilation and testing of the features. The main function interfacing the precedure should have the following expression

`double calc(char* expression);`

# Methodology of solution

To solve the requested problem, the author has decided to convert the mathematical expression string into a reverse polish notation string, and then use it to perform the calculations in a more standard manner. To generate the rpn string and to use it to calculate the provided expression, the author has used two types of data structres:

- Operators Stack: the implementation is based on a singly linked list, which stores pointers from bottom to top.
- RPN Stack: the implementation is based on a doubly linked list, with pointers from bottom to top and the other way around. However, given the peculiarity of the problem, it has been decided to construct the stack maintaining only the bottom-down references, and construct the references base-up only after the stack has the whole RPN string represented in it. In fact, the base-up references need to be constructed only to perform the calculations represented by the stack

# Runing the code

To compile the code, linux *makefile* files are provided. In the main source directory, one should execute:

> make

this command should compile the main source code. To test the solution, one can execute the following in the `<MAIN_SRC_DIR>/tests/` directory:

> make && make run

