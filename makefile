COMPILER = gcc
CFLAGS 	 = -I./src -lm -Wall -Wextra

all: ./obj/util.o ./obj/op_stack.o ./obj/rpn_stack.o
	mkdir -p obj
	make ./obj/util.o
	make ./obj/op_stack.o
	make ./obj/rpn_stack.o

./obj/util.o: ./src/util.c ./src/util.h
	$(COMPILER) $(CFLAGS) -c ./src/util.c -o ./obj/util.o

./obj/op_stack.o: ./src/op_stack.c ./src/op_stack.h
	$(COMPILER) $(CFLAGS) -c ./src/op_stack.c -o ./obj/op_stack.o

./obj/rpn_stack.o: ./src/rpn_stack.c ./src/rpn_stack.h
	$(COMPILER) $(CFLAGS) -c ./src/rpn_stack.c -o ./obj/rpn_stack.o

clean:
	rm ./obj/*.o
