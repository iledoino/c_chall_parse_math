#include "op_stack.h"
#include "util.h"
#include <stdio.h>

/*
 * Operators stack implemented using singly linked list
 */

OpStack*
op_stack_create()
{
    OpStack* op_st_ptr = (OpStack*) malloc(sizeof(OpStack));
    (*op_st_ptr) = (OpStack){ .bottom_ptr = NULL, .op = BASE };
    return op_st_ptr;
}

void
op_stack_push(OpStack** ptr_ptr, const int op)
{
    OpStack* top_ptr = (OpStack*) malloc(sizeof(OpStack));
    top_ptr->bottom_ptr = *ptr_ptr;
    top_ptr->op = op;
    *ptr_ptr = top_ptr;
}

void
op_stack_pop(OpStack** ptr_ptr)
{
    if (*ptr_ptr == NULL)
        printf("op_stack_pop: provided pointer is NULL");

    OpStack* temp = *ptr_ptr;
    *ptr_ptr = (*ptr_ptr)->bottom_ptr;
    free(temp);
}

void
op_stack_destruct(OpStack** ptr_ptr)
{
    while (((*ptr_ptr) != NULL) && ((*ptr_ptr)->bottom_ptr != NULL))
        op_stack_pop(ptr_ptr);
    if ((*ptr_ptr) != NULL) {
        if ((*ptr_ptr)->op != BASE)
            printf("Error at deleting OpStack: found %c op", (*ptr_ptr)->op);
        op_stack_pop(ptr_ptr);
    }
    *ptr_ptr = NULL;
}
