#include "rpn_stack.h"
#include "op_stack.h"
#include "util.h"
#include <stdio.h>

/*
 * Reverse Polish Notation stack
 *
 * PS: the stack is build from bottom up, and once it is finished,
 * the function rpn_stack_construct_top_ptr must be called to construct
 * pointers from the top to bottom
 */
struct rpn_stack
{
    struct rpn_stack* bottom_ptr;
    struct rpn_stack* top_ptr;
    double num;
    int code;
};

int
rpn_stack_check(RPNStack** ptr_ptr)
{
    if (ptr_ptr == NULL)
        return 0;

    if (*ptr_ptr == NULL)
        return 0;

    return 1;
}

RPNStack*
rpn_stack_create()
{
    RPNStack* rpn_st_ptr = (RPNStack*) malloc(sizeof(RPNStack));
    (*rpn_st_ptr) = (RPNStack){
        .bottom_ptr = NULL, .top_ptr = NULL, .num = 0.0, .code = BASE
    };
    return rpn_st_ptr;
}

void
rpn_stack_push(RPNStack** ptr_ptr, const double number, const int code)
{
    if (ptr_ptr == NULL)
        return;

    RPNStack* top_ptr = (RPNStack*) malloc(sizeof(RPNStack));
    top_ptr->bottom_ptr = *ptr_ptr;
    top_ptr->num = number;
    top_ptr->code = code;
    *ptr_ptr = top_ptr;
}

void
rpn_stack_pop(RPNStack** ptr_ptr)
{
    if (!rpn_stack_check(ptr_ptr))
        return;

    RPNStack* temp = *ptr_ptr;
    *ptr_ptr = (*ptr_ptr)->bottom_ptr;
    free(temp);
}

void
rpn_stack_construct_top_ptr(RPNStack** ptr_ptr)
{
    if (!rpn_stack_check(ptr_ptr))
        return;

    RPNStack* tail_ptr = (*ptr_ptr);
    RPNStack* base_ptr = (*ptr_ptr)->bottom_ptr;

    base_ptr->top_ptr = tail_ptr;
    while ((base_ptr != NULL) && (base_ptr->code != BASE)) {
        base_ptr = base_ptr->bottom_ptr;
        tail_ptr = tail_ptr->bottom_ptr;
        base_ptr->top_ptr = tail_ptr;
    }
    return;
}

/*
 * Assuming the reference passed points to an operator, the function
 * pops the two numbers right below the rpn stack and replaces the operator
 * entry by a number entry with the result of the operation between the
 * two numbers
 */
int
rpn_stack_operate(RPNStack** ptr_ptr)
{
    if (!rpn_stack_check(ptr_ptr))
        return 0;

    RPNStack* rpn_st_ptr = *ptr_ptr;

    if (rpn_st_ptr->code == NUM)
        return 1;

    double a, b;
    RPNStack* temp = rpn_st_ptr->bottom_ptr;

    if (temp != NULL) {
        b = temp->num;
        rpn_st_ptr->bottom_ptr = temp->bottom_ptr;
        (temp->bottom_ptr)->top_ptr = rpn_st_ptr;
        free(temp);
        temp = rpn_st_ptr->bottom_ptr;
    }
    else {
        // Current top is a number but there is nothing to do
        return 2;
    }
    if (temp != NULL) {
        a = (rpn_st_ptr->bottom_ptr)->num;
        rpn_st_ptr->bottom_ptr = temp->bottom_ptr;
        (temp->bottom_ptr)->top_ptr = rpn_st_ptr;
        free(temp);
        temp = rpn_st_ptr->bottom_ptr;
    }
    else {
        // There are no two numbers to make the computation
        // Assume the identity of the operation for the second
        if (is_l1_op_f(rpn_st_ptr->code))
            a = 0.0;
        else
            a = 1.0;
    }

    switch (rpn_st_ptr->code) {
    case SUM:
        rpn_st_ptr->num = a + b;
        break;
    case SUB:
        rpn_st_ptr->num = a - b;
        break;
    case PRO:
        rpn_st_ptr->num = a * b;
        break;
    case DIV:
        rpn_st_ptr->num = a / b;
        break;
    }
    rpn_st_ptr->code = NUM;

    return 3;
}

/*
 * Destruct the stack from bottom to top
 */
void
rpn_stack_destruct_bottom_down(RPNStack** ptr_ptr)
{
    if (!rpn_stack_check(ptr_ptr))
        return;

    while (((*ptr_ptr) != NULL) && ((*ptr_ptr)->bottom_ptr != NULL))
        rpn_stack_pop(ptr_ptr);
    if ((*ptr_ptr) != NULL) {
        if ((*ptr_ptr)->code != BASE)
            printf("Error at deleting RPNStack: found %c code",
                    (*ptr_ptr)->code);
        rpn_stack_pop(ptr_ptr);
    }
    *ptr_ptr = NULL;
}

/*
 * Destruct the stack from base to top
 * PS: the function goes from base to top and then destructs everything
 * from there down using rpn_stack_destruct_bottom_down function
 */
void
rpn_stack_destruct_base_up(RPNStack** ptr_ptr)
{
    if (!rpn_stack_check(ptr_ptr))
        return;

    RPNStack* ptr = (*ptr_ptr);

    while (ptr->top_ptr != NULL)
        ptr = ptr->top_ptr;

    rpn_stack_destruct_bottom_down(&ptr);
    *ptr_ptr = NULL;
}

void
push_l2_op(OpStack** op_st_ptr_ptr, RPNStack** rpn_st_ptr_ptr, int op)
{
    OpStack* op_st_ptr = *op_st_ptr_ptr;
    while (is_l2_op_f(op_st_ptr->op)) {
        rpn_stack_push(rpn_st_ptr_ptr, 0.0, op_st_ptr->op);
        op_stack_pop(&op_st_ptr);
    }
    op_stack_push(&op_st_ptr, op);
    *op_st_ptr_ptr = op_st_ptr;
}

/*
 * Given a valid string operation, the function converts it into reverse
 * polish notation
 */
void
get_rpn_stack(RPNStack** ptr_ptr, char* expression)
{
    if (!rpn_stack_check(ptr_ptr))
        return;

    double number;
    RPNStack* rpn_st_ptr = *ptr_ptr;
    OpStack* op_st_ptr = op_stack_create();

    char* p = expression;
    int last_code = BASE;
    int push_lpar_neg = 0;
    while (*p) {
        if (((*p) != ' ')) {
            int op = char_2_code(*p);
            int is_l1_op = is_l1_op_f(op);
            int do_sings_rule = is_l1_op && is_l1_op_f(last_code);
            int l2_l1_signs = is_l1_op && is_l2_op_f(last_code);
            int read_double = read_double_f(*p)
                    || (l2_l1_signs && read_double_f(*(p + 1)));

            if (do_sings_rule) {
                op_st_ptr->op = (op_st_ptr->op == op ? SUM : SUB);
                last_code = op;
            }
            else if (read_double) {
                sscanf(p, "%lf", &number);
                rpn_stack_push(&rpn_st_ptr, number, NUM);
                last_code = NUM;
                if (number < 0)
                    p++;
                while (((*(p + 1)) != '\0') && read_double_f(*(p + 1)))
                    p++;
            }
            else if (((*p) == '(')) {
                int op_to_push = (push_lpar_neg ? LPAR_NEG : LPAR);
                op_stack_push(&op_st_ptr, op_to_push);
                last_code = op_to_push;
                push_lpar_neg = 0;
            }
            else if (((*p) == ')')) {
                while ((op_st_ptr != NULL) && (op_st_ptr->op != LPAR)
                        && (op_st_ptr->op != LPAR_NEG)
                        && (op_st_ptr->op != BASE)) {
                    rpn_stack_push(&rpn_st_ptr, 0.0, op_st_ptr->op);
                    op_stack_pop(&op_st_ptr);
                }
                int is_neg_lpar = op_st_ptr->op == LPAR_NEG;
                if (is_neg_lpar || op_st_ptr->op == LPAR)
                    op_stack_pop(&op_st_ptr);
                // in the case of negation of parenthesis, multiply by -1
                if (is_neg_lpar) {
                    push_l2_op(&op_st_ptr, &rpn_st_ptr, PRO);
                    rpn_stack_push(&rpn_st_ptr, 1.0, NUM);
                    last_code = NUM;
                }
                else
                    last_code = RPAR;
            }
            else {
                // last rpn is either number or right parenthesis or an l2 op
                int is_l2_op = is_l2_op_f(op);

                if (is_l1_op) {
                    int push_zero = (last_code == LPAR)
                            || (last_code == LPAR_NEG) || (last_code == BASE);
                    if (push_zero) {
                        rpn_stack_push(&rpn_st_ptr, 0.0, NUM);
                        last_code = NUM;
                    }
                    push_lpar_neg
                            = l2_l1_signs && next_is_lpar(p) && (op == SUB);
                    if (!push_lpar_neg) {
                        while (is_l2_op_f(op_st_ptr->op)
                                || is_l1_op_f(op_st_ptr->op)) {
                            rpn_stack_push(&rpn_st_ptr, 0.0, op_st_ptr->op);
                            op_stack_pop(&op_st_ptr);
                        }
                        op_stack_push(&op_st_ptr, op);
                    }
                }
                else if (is_l2_op) {
                    push_l2_op(&op_st_ptr, &rpn_st_ptr, op);
                }
                last_code = op;
            }
        }
        ++p;
    }
    while ((op_st_ptr != NULL) && (op_st_ptr->bottom_ptr != NULL)) {
        rpn_stack_push(&rpn_st_ptr, 0.0, op_st_ptr->op);
        op_stack_pop(&op_st_ptr);
    }
    rpn_stack_construct_top_ptr(&rpn_st_ptr);

    op_stack_destruct(&op_st_ptr);
}

/*
 * Given a reverse polish notation, the function executes the mathematical
 * operations it represents, and then returns the result
 */
void
calc_from_rpn(RPNStack** ptr_ptr)
{
    if (!rpn_stack_check(ptr_ptr))
        return;

    RPNStack* tail_ptr = (*ptr_ptr)->top_ptr;
    RPNStack* base_ptr = *ptr_ptr;

    while ((base_ptr != NULL) && (tail_ptr->top_ptr != NULL)) {
        while ((tail_ptr != NULL) && (tail_ptr->code == NUM)) {
            tail_ptr = tail_ptr->top_ptr;
            base_ptr = base_ptr->top_ptr;
        }
        if (tail_ptr->code != NUM) {
            rpn_stack_operate(&tail_ptr);
            base_ptr = tail_ptr->bottom_ptr;
        }
    }
}

/*
 * Given a valid string mathematical expression, the function converts it
 * into reverse polish notation and then uses it to execute the expression,
 * returning the result
 */
double
calc(char* expression)
{
    RPNStack* rpn_st_ptr = rpn_stack_create();

    get_rpn_stack(&rpn_st_ptr, expression);
    calc_from_rpn(&rpn_st_ptr);

    double result = 0.;
    if (rpn_st_ptr != NULL) {
        if (rpn_st_ptr->top_ptr != NULL)
            result = rpn_st_ptr->top_ptr->num;
    }

    rpn_stack_destruct_base_up(&rpn_st_ptr);

    return result;
}
