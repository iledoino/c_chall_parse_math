#ifndef SRC_UTIL_H
#define SRC_UTIL_H

#include <ctype.h>
#include <stdlib.h>

#define NOT_A_CODE -3
#define RPAR -2
#define BASE -1
#define NUM 0
#define SUM 1
#define SUB 2
#define PRO 3
#define DIV 4
#define LPAR 5
#define LPAR_NEG 6

int char_2_code(const char c);

char code_2_char(const int c);

int is_l1_op_f(const int op);

int is_l2_op_f(const int op);

int read_double_f(char c);

int next_is_lpar(char* p);

#endif /* SRC_UTIL_H */
