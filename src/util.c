#include "util.h"

int
char_2_code(const char c)
{
    if (c == '(')
        return LPAR;
    if (c == ')')
        return RPAR;
    if (c == '+')
        return SUM;
    if (c == '-')
        return SUB;
    if (c == '*')
        return PRO;
    if (c == '/')
        return DIV;
    return BASE;
}

char
code_2_char(const int c)
{
    if (c == LPAR)
        return '(';
    if (c == RPAR)
        return ')';
    if (c == SUM)
        return '+';
    if (c == SUB)
        return '-';
    if (c == PRO)
        return '*';
    if (c == DIV)
        return '/';
    return '\n';
}

int
is_l1_op_f(const int op)
{
    return (op == SUM) || (op == SUB);
}

int
is_l2_op_f(const int op)
{
    return (op == PRO) || (op == DIV);
}

int
read_double_f(char c)
{
    return isdigit(c) || (c == '.');
}

int
next_is_lpar(char* p)
{
    while (*(++p) && (*p) == ' ')
        ;
    return *p == '(';
}
