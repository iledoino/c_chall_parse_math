#ifndef SRC_RPN_STACK_H
#define SRC_RPN_STACK_H

#include <stdlib.h>

/*
 * Reverse Polish Notation stack
 */
typedef struct rpn_stack RPNStack;

int rpn_stack_check(RPNStack** ptr_ptr);

RPNStack* rpn_stack_create();

void rpn_stack_push(RPNStack** ptr_ptr, const double number, const int code);

void rpn_stack_pop(RPNStack** ptr_ptr);

void rpn_stack_construct_top_ptr(RPNStack** ptr_ptr);

/*
 * Assuming the reference passed points to an operator, the function
 * pops the two numbers right below the rpn stack and replaces the operator
 * entry by a number entry with the result of the operation between the
 * two numbers
 */
int rpn_stack_operate(RPNStack** ptr_ptr);

/*
 * Destruct the stack from top to bottom
 */
void rpn_stack_destruct_bottom_down(RPNStack** ptr_ptr);

/*
 * Destruct the stack from base to top
 */
void rpn_stack_destruct_base_up(RPNStack** ptr_ptr);

/*
 * Given a valid string operation, the function converts it into reverse
 * polish notation
 */
void get_rpn_stack(RPNStack** ptr_ptr, char* expression);

/*
 * Given a reverse polish notation, the function executes the mathematical
 * operations it represents, and then returns the result
 */
void calc_from_rpn(RPNStack** ptr_ptr);

/*
 * Given a valid string mathematical expression, the function converts it
 * into reverse polish notation and then uses it to execute the expression,
 * returning the result
 */
double calc(char* expression);

#endif /* SRC_RPN_STACK_H */
