#ifndef SRC_OP_STACK_H
#define SRC_OP_STACK_H

#include <stdlib.h>

/*
 * Operators stack
 */
struct op_stack
{
    struct op_stack* bottom_ptr;
    int op;
};
typedef struct op_stack OpStack;

OpStack* op_stack_create();

void op_stack_push(OpStack** ptr_ptr, const int op);

void op_stack_pop(OpStack** ptr_ptr);

void op_stack_destruct(OpStack** ptr_ptr);

#endif /* SRC_OP_STACK_H */
