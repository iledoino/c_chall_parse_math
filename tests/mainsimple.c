#include "rpn_stack.h"
#include <stdio.h>
#include <stdlib.h>

int
main() // int argc, char** argv)
{
    // char expression[] = "3 + 4 * 2 / ( 1 - 5 ) ";
    // char expression[] = "3 + 4 * 2 /  1 - 5  ";
    // char expression[] = "3 + (4 - 2*(1 - 5)/5) + 1  ";
    // char expression[] = "1-1";
    // char expression[] = "1 -1";
    // char expression[] = "1- 1";
    // char expression[] = "1 - 1";
    // char expression[] = "1- -1 ";
    // char expression[] = "1 - -1";
    // char expression[] = "6 + -(4)";
    // char expression[] = "6 + -( -4)";
    // char expression[] = "-4+1";
    // char expression[] = ".1+1-1.1";
    // char expression[] = "-.1+-(1-1.1)";
    // char expression[] = "(123.45*(678.90 / (-2.5+ 11.5)-(((80 -(19)))*33.25))
    // / 20) - (123.45*(678.90 / (-2.5+ 11.5)-(((80 -(19))) *33.25)) /20) + (13
    // - 2)/ -(-11)";
    char expression[] = "12* 123/-(-5 + 2)";

    printf("The result is %lf\n", calc(expression));

    return 0;
}
